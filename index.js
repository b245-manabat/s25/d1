// [SECTION] JSON Objects
	// JSON stands for JavaScript Object Notation
	// JSON is laso used in other programming languages
	// core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript Objects
		// JSON Obects - it means parsed
		// Stringified JSON Object - JSON Objects na nakastringify
	// JSON is used for serializing/converting different data types
	/*
		Syntax/Format:
			{
				"propertyA" : "valueA",
				"propertyB" : "valueB",
				. . .
			}
	*/

	// JSON Object
/*	{
		"city" : "Quezon City",
		"province" : "Manila",
		"country" : "Philippines"
	}*/

	// JSON Arrays
		// array of JSON Objects
/*	[
		{
		"city" : "Quezon City",
		"province" : "Manila",
		"country" : "Philippines"
		}
		{
		"city" : "Manila City",
		"province" : "Manila",
		"country" : "Philippines"
		}
	]*/

// [SECTION] JSON Methods
	// JSON methods contains methods for parsing and converting data into stringified JSON

	let batchesArr = [
			{
				batchName: "Batch X"
			},
			{
				batchName: "Batch Y"
			}

		]

	console.log("This is the original array:");
	console.log(batchesArr);

	// stringify method is used to convert JavaScript Objects/Arrays into a string.
	let stringBatchesArr = JSON.stringify(batchesArr);

	console.log("This is the result of stringify method:");
	console.log(stringBatchesArr);

	// to check the data type of the array after the stringify method
	console.log("Data Type:");
	console.log(typeof stringBatchesArr);

	console.log("This is the original array:");
	console.log(batchesArr);

	let data = JSON.stringify({
		name: 'john',
		address: {
			city: 'manila',
			country: 'philippines'
		}
	})

	console.log(data);

// [SECTION] Use stringify methid with variables
	// when information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable.

	let firstName = 'lebron';
	let lastName = 'james';
	let age = 38;
	let address = {
		city : "manila",
		country : "ph"
	}

	let otherData = {
		firstName,
		lastName,
		age,
		address
	}

	console.log(otherData);

// [SECTION] Converting stringified JSON into JavaScript Objects
	/*
		- objects are common data types used in application because of the complex data structures that can be created our of them.
		- information is commonly sent to application in stringiied JSON and then converted back into objects
		- this happens both for sending information to a backend application and sending nformation back to frontend application

	*/

	// parse method converts the stringify JSON into JSON object
	let objectBatchesArr = JSON.parse(stringBatchesArr);
	console.log("This is the stringify version:");
	console.log(stringBatchesArr);
	console.log("This is the result after the parse method version:");
	console.log(objectBatchesArr);
	console.log(typeof objectBatchesArr);

	console.log(objectBatchesArr[0]);

	let stringifiedObject = `{
		"name" : "John",
		"age" : 31,
		"address" : {
			"city" : "Manila City",
			"country" : "Philippines"
		}
	}`

	console.log(JSON.parse(stringifiedObject));